package robotname

type Robot struct {
	name string
}

func (r *Robot) Name() string {
	return r.name
}

func (r *Robot) Reset() {
	rName := r.name
	if rName != "" {
		r.name = ""
	}
}

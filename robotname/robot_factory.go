package robotname

import (
	"errors"
	"fmt"
	"log"
	"math"
	"math/rand"
	"strings"
	"sync"
	"time"
)

type RobotFactory struct {
	CombinationsSlice    []string
	totalCombinations    int
	maxLetterCombination float64
	maxDigitCombination  float64
	runeA                rune
	runeZ                rune
	names                string
	mtx                  *sync.Mutex
}

// 2, 3
func NewRobotFactory(letters, digits int) *RobotFactory {
	rf := &RobotFactory{
		runeA: rune('a'),
		runeZ: rune('z'),
		mtx:   &sync.Mutex{},
	}

	return rf.init(letters, digits)
}

func (rf *RobotFactory) init(letters, digits int) *RobotFactory {
	rand.Seed(time.Now().Unix())
	rf.totalCombinations = rf.maxCombinations(letters, digits)
	rf.CombinationsSlice = make([]string, rf.totalCombinations)
	rf.setCombinations(digits)
	return rf
}

func (rf *RobotFactory) maxCombinations(letters int, digits int) int {
	rf.maxLetterCombination = math.Pow(float64(1+int(rf.runeZ)-int(rf.runeA)), float64(letters))
	rf.maxDigitCombination = math.Pow(10, float64(digits))
	return int(rf.maxLetterCombination * rf.maxDigitCombination)
}

func (rf *RobotFactory) setCombinations(digits int) {
	count := 0
	for i := 0; i <= int(rf.maxDigitCombination)-1; i++ {
		for j := rf.runeA; j < rf.runeZ+1; j++ {
			for k := rf.runeA; k < rf.runeZ+1; k++ {

				rf.CombinationsSlice[count] = strings.ToUpper(fmt.Sprintf("%s%s%s", string(k), string(j), padLeftWithZeros(i, digits)))
				count++
			}
		}
	}
}

func (rf *RobotFactory) GetNames() string {
	return rf.names
}

func (rf *RobotFactory) LogIntoFile(wg *sync.WaitGroup, rb *Robot) error {
	defer wg.Done()
	log.Println(rb.name)
	return nil
}

func (rf *RobotFactory) NewRobotWithName() (*Robot, error) {
	rb := &Robot{}
	n, err := rf.getNewName()
	if err != nil {
		return nil, err
	}

	rb.name = n
	return rb, nil
}

func (rf *RobotFactory) ResetName(rb *Robot) *Robot {
	n, err := rf.getNewName()
	if err != nil {
		log.Fatal(err)
		return nil
	}

	rb.name = n
	return rb
}

func (rf *RobotFactory) TotalCombinations() int {
	return rf.totalCombinations
}

func (rf *RobotFactory) getNewName() (string, error) {
	name, err := rf.getRandomName()
	if err != nil {
		log.Printf("getRandonName: %s\n", err)
		return "", err
	}
	return name, nil
}

func (rf *RobotFactory) getRandomName() (string, error) {
	if len(rf.CombinationsSlice) <= 0 {
		return "", errors.New("no more combinations available")
	}

	idx := getRandomNumberOfRange(0, len(rf.CombinationsSlice))
	name := rf.CombinationsSlice[idx]
	rf.CombinationsSlice = remove(rf.CombinationsSlice, idx)
	return name, nil
}

func getRandomNumberOfRange(min, max int) int {
	return min + rand.Intn(max-min)
}

// padLeftWithZeros number is the number to format, padWith is how many zeros will be path to the left.
func padLeftWithZeros(number, padWidth int) string {
	return fmt.Sprintf(fmt.Sprintf("%%0%dd", padWidth), number)
}

func remove(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

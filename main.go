package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"gitlab.com/urkob/robotname/pkg/util"
	"gitlab.com/urkob/robotname/robotname"
)

func main() {
	tm := util.Timer("main")
	defer tm()
	logFileName := fmt.Sprintf("%s.txt", time.Now().Format(strings.ReplaceAll(time.RFC1123Z, ":", "_")))
	f, err := os.OpenFile(logFileName, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0o644)
	if err != nil {
		log.Fatal(err)
	}

	log.SetFlags(log.Lmicroseconds)
	defer f.Close()
	log.SetOutput(f)

	wg := &sync.WaitGroup{}

	rf := robotname.NewRobotFactory(2, 3)
	for i := 0; i < rf.TotalCombinations(); i++ {
		rb, err := rf.NewRobotWithName()
		if err != nil {
			log.Fatalf("rf.NewRobotWithName: %s\n", err)
			return
		}
		wg.Add(1)
		go rf.LogIntoFile(wg, rb)
	}
	wg.Wait()
}
